package com.example.essentialandroid2.exp3;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.example.essentialandroid2.R;

public class ExampleActivity extends AppCompatActivity {

    TextView textView1, textView2, textView3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_example);
        tanimla();
        al();
    }


    public void tanimla() {

        textView1 = findViewById(R.id.text1);
        textView2 = findViewById(R.id.text2);
        textView3 = findViewById(R.id.text3);


    }

    public void al() {

        Bundle bundle = getIntent().getExtras();
        String isim = bundle.getString("isim");
        String soyIsim = bundle.getString("soyIsim");
        String numara = bundle.getString("numara");

        textView1.setText(isim);
        textView2.setText(soyIsim);
        textView3.setText(numara);

    }
}
