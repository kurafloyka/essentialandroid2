package com.example.essentialandroid2.exp3;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.essentialandroid2.R;

import java.util.List;

public class AdapterUser extends BaseAdapter {
    List<User> userList;
    Context context;
    Activity activity;

    public AdapterUser(List<User> userList, Context context, Activity activity) {
        this.userList = userList;
        this.context = context;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return userList.size();
    }

    @Override
    public Object getItem(int position) {
        return userList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        convertView = LayoutInflater.from(context).inflate(R.layout.layout, parent, false);

        TextView isim = convertView.findViewById(R.id.isim);
        TextView soyisim = convertView.findViewById(R.id.soyisim);
        TextView numara = convertView.findViewById(R.id.telNo);

        LinearLayout linearLayout = convertView.findViewById(R.id.anaLayout);


        final String isims = userList.get(position).getIsim();
        final String soyIsimS = userList.get(position).getSoyIsim();
        final String numaraS = userList.get(position).getNumara();

        isim.setText(isims);
        soyisim.setText(soyIsimS);
        numara.setText(numaraS);

        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ExampleActivity.class);
                intent.putExtra("isim", isims);
                intent.putExtra("soyIsim", soyIsimS);
                intent.putExtra("numara", numaraS);
                activity.startActivity(intent);

            }
        });

        return convertView;
    }
}
