package com.example.essentialandroid2.exp3;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ListView;

import com.example.essentialandroid2.R;

import java.util.ArrayList;
import java.util.List;

public class ListViewActivity extends AppCompatActivity {

    List<User> userList;
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view);
        listeOlustur();
        gec();
    }


    public void listeOlustur() {

        User u1 = new User("Faruk", "AKYOL", "2343242");
        User u2 = new User("Ahmet", "Kirtas", "324556");
        User u3 = new User("Fatih", "Akyol", "5055423");
        User u4 = new User("Elif", "Tas", "493871");

        userList = new ArrayList<>();

        userList.add(u1);
        userList.add(u2);
        userList.add(u3);
        userList.add(u4);

    }

    public void gec() {
        listView = findViewById(R.id.list_view);
        AdapterUser adapterUser = new AdapterUser(userList, this, this);
        listView.setAdapter(adapterUser);
    }
}
