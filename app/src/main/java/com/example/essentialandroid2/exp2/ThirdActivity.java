package com.example.essentialandroid2.exp2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.example.essentialandroid2.R;

public class ThirdActivity extends AppCompatActivity {


    EditText userName, password;
    Button btn;
    RadioGroup rdn;

    String kullaniciAdi, kullaniciSifre, kullaniciCinsiyet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);
        tanimla();
        tiklaGec();
    }

    public void tanimla() {
        userName = findViewById(R.id.editTextKullanici);
        password = findViewById(R.id.editTextSifre);
        btn = findViewById(R.id.gonderButon);
        rdn = findViewById(R.id.CinsiyetGrup);

    }


    public void degerAl() {

        kullaniciAdi = userName.getText().toString();
        kullaniciSifre = password.getText().toString();
        kullaniciCinsiyet = ((RadioButton) findViewById(rdn.getCheckedRadioButtonId())).getText().toString();

    }

    void tiklaGec() {


        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                degerAl();
                Log.i("deger", kullaniciAdi + kullaniciSifre + kullaniciCinsiyet);
                Intent intent = new Intent(getApplicationContext(), FourthActivity.class);
                intent.putExtra("kullaniciAdi", kullaniciAdi);
                intent.putExtra("kullaniciSifre", kullaniciSifre);
                intent.putExtra("kullaniciCinsiyet", kullaniciCinsiyet);
                startActivity(intent);
            }
        });
    }
}
