package com.example.essentialandroid2.exp2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.example.essentialandroid2.R;

public class FourthActivity extends AppCompatActivity {
    String kadi, ksifre, kcinsiyet;

    TextView adiView, sifreView, cinsiyetView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fourth);

        getInfo();
        define();


    }


    public void getInfo() {


        Bundle intent = getIntent().getExtras();

        kadi = intent.getString("kullaniciAdi");
        ksifre = intent.getString("kullaniciSifre");
        kcinsiyet = intent.getString("kullaniciCinsiyet");

        Log.i("Ikinci frame", kadi + ksifre + kcinsiyet);
    }


    public void define() {

        adiView = findViewById(R.id.adi);
        sifreView = findViewById(R.id.sifre);
        cinsiyetView = findViewById(R.id.cinsiyet);

        adiView.setText(adiView.getText() + " " + kadi);
        sifreView.setText(sifreView.getText() + " " + ksifre);
        cinsiyetView.setText(cinsiyetView.getText() + " " + kcinsiyet);

    }
}
