package com.example.essentialandroid2.progressdialog;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;

import com.example.essentialandroid2.R;

public class ProgressDialogActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_progress_dialog);
        progressDialogVer();
    }

    public void progressDialogVer() {


        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Mesajlar Bolumu");
        progressDialog.setMessage("Mesajlar listeleniyor lutfen bekleyiniz");
        progressDialog.setCancelable(false);
        progressDialog.show();
        new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    Thread.sleep(20000);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                progressDialog.cancel();
            }
        }).start();
    }
}
