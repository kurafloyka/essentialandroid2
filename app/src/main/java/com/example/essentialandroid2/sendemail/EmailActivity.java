package com.example.essentialandroid2.sendemail;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.essentialandroid2.R;

public class EmailActivity extends AppCompatActivity {

    EditText icerik, konu, mailAdres;
    Button button;
    String icerikText, konuText, mailAdresiText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email);
        tanimla();
        mailGonder();


    }

    public void tanimla() {
        icerik = findViewById(R.id.editTextMailIcerik);
        konu = findViewById(R.id.editTextMailKonusu);
        mailAdres = findViewById(R.id.editTextEmailText);
        button = findViewById(R.id.mailAt);
    }


    public void bilgiAl() {
        icerikText = icerik.getText().toString();
        konuText = konu.getText().toString();
        mailAdresiText = mailAdres.getText().toString();
    }

    public void mailGonder() {


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bilgiAl();


                try {
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.setType("message/rfc822");
                    intent.putExtra(Intent.EXTRA_EMAIL, mailAdresiText);
                    intent.putExtra(Intent.EXTRA_SUBJECT, konuText);
                    intent.putExtra(Intent.EXTRA_TEXT, icerikText);
                    startActivity(Intent.createChooser(intent, "Mail Gonderiniz...!!!"));
                } catch (Exception e) {

                    System.out.println("Email parametreleri set edilirken hata alindi...." + e);
                }
            }
        });
    }
}
