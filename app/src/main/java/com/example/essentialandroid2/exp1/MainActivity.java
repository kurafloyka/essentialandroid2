package com.example.essentialandroid2.exp1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.essentialandroid2.R;

public class MainActivity extends AppCompatActivity {

    Button button;

    public void tanimla() {
        button = findViewById(R.id.buttonIntent);

    }

    public void goSecondActivity() {

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gecisYap();
            }
        });

    }

    public void gecisYap() {
        Intent intent = new Intent(this, SecondActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tanimla();
        goSecondActivity();
        Log.i("Takip", "OnCreate metodu" + " ----- uygulama acilirken kullanilir");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i("Takip", "OnStart metodu" + " ----- uygulama acilirken kullanilir");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("Takip", "OnResume metodu" + " ----- uygulama acilirken kullanilir");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i("Takip", "OnStop metodu" + " ----- Simge boyutuna alinca calisti");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i("Takip", "OnPause metodu" + " ----- Simge boyutuna alinca calisti");
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i("Takip", "OnRestart metodu" + " ----- uygulama normal boyuta gelince kullanilir");
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i("Takip", "OnDestroy metodu" + " ----- uygulama kapatilirken kullanilir");
    }


}
