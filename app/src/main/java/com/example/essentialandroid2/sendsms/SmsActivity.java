package com.example.essentialandroid2.sendsms;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ListView;

import com.example.essentialandroid2.R;

import java.util.ArrayList;
import java.util.List;

public class SmsActivity extends AppCompatActivity {
    List<Message> list;
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sms);
        listeDoldur();
        tanimla();
    }


    public void listeDoldur() {

        Message m1 = new Message("Faruk AKYOL", "5455430080");
        Message m2 = new Message("AHMET AKYOL", "4342322432");
        Message m3 = new Message("fatih akyol", "322424242");
        Message m4 = new Message("Elif akyol", "3242222424");
        Message m5 = new Message("Sedat yilmaz", "243242121");

        list = new ArrayList<>();
        list.add(m1);
        list.add(m2);
        list.add(m3);
        list.add(m4);
        list.add(m5);
    }

    public void tanimla() {

        listView = findViewById(R.id.mesajList);

        AdapterMessage adapterMessage = new AdapterMessage(list, this, this);
        listView.setAdapter(adapterMessage);


    }
}
