package com.example.essentialandroid2.alertdialog;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.essentialandroid2.R;

public class AlertActivity extends AppCompatActivity {
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alert);
        tanimla();

    }

    public void tanimla() {
        button = findViewById(R.id.dialogAc);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogAc();
            }
        });
    }

    public void dialogAc() {

        LayoutInflater layoutInflater = getLayoutInflater();
        View view = layoutInflater.inflate(R.layout.alertlayout, null);

        final EditText mailEditText = view.findViewById(R.id.mailAdres);
        final EditText kadiEditText = view.findViewById(R.id.kullanici);
        final EditText sifreEditText = view.findViewById(R.id.sifre);
        Button button = view.findViewById(R.id.uyeOlButonu);
        Button iptalButton = view.findViewById(R.id.iptalEt);

        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setView(view);
        alert.setCancelable(false);
        final AlertDialog dialog = alert.create();

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), mailEditText.getText().toString()
                        + " " + kadiEditText.getText().toString() + " "
                        + sifreEditText.getText().toString(), Toast.LENGTH_LONG).show();
                dialog.cancel();
            }

        });

        iptalButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
        dialog.show();
    }
}
